$(document).ready(function () {

  $('#s-special-offers-announcement-slider').owlCarousel({
    items: 1,
    dots: false,
    margin: 10,
    nav: true,
    navText: ['<svg width="20px" height="36px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="20px" height="36px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
  });

  $('#promo-slider').owlCarousel({
    items: 1,
    dots: false,
    margin: 30
  });

  $('#partners').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 400,
    dots: false,
    responsive: {
      0: {
        items: 2
      },
      768: {
        items: 3
      },
      992: {
        items: 4
      },
      1200: {
        items: 6
      }
    }
  });

  $('#new-products-slider').owlCarousel({
    loop: true,
    dots: false,
    navText: ['<svg width="50px" height="70px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="50px" height="70px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    responsive: {
      0: {
        items: 1,
        stagePadding: 50,
        margin: 20
      },
      448: {
        items: 2,
        stagePadding: 40,
        margin: 20
      },
      768: {
        items: 3,
        margin: 20,
        nav: true
      },
      992: {
        items: 3,
        margin: 30,
        nav: true
      }
    }
  });

  $('#sale-products-slider').owlCarousel({
    loop: true,
    dots: false,
    navText: ['<svg width="50px" height="70px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="50px" height="70px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    responsive: {
      0: {
        items: 1,
        stagePadding: 50,
        margin: 20
      },
      448: {
        items: 2,
        stagePadding: 40,
        margin: 20
      },
      768: {
        items: 3,
        margin: 20,
        nav: true
      },
      992: {
        items: 3,
        margin: 30,
        nav: true
      }
    }
  });

  $('#s-gallery-thumbs').owlCarousel({
    nav: false,
    dots: false,
    loop: true,
    center: true,
    margin: 5,
    responsive: {
      0: {
        items: 3,
      }
    }
  });

  $('.s-gallery__thumbs').on('click', 'img', function () {
    var carousel = $('.s-gallery__thumbs').data('owl.carousel');
    carousel.to(carousel.relative($(this).parent().parent().index()), false, true);
  });

  $('.s-gallery__thumbs').on('changed.owl.carousel', function () {
    setTimeout(function (){
      var img = $('.s-gallery__thumbs').find('.owl-item.center div img').attr('src');
      $('.s-gallery__cover-image-full').attr('src', img);
    }, 100);
  });

});
