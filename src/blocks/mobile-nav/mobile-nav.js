$(document).ready(function() {

  $('.s-mobile-nav__item--has-child').each(function () {
    $(this).find('.s-mobile-nav__link').on('click', function (e) {
      e.preventDefault();
      $(this).parent().find('ul').toggleClass('is-open');
      $(this).toggleClass('s-mobile-nav__item--sublist-is-open');
    })
  });

});